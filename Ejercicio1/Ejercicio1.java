/*Codifique una clase que dado una cadena de caracteres en minúscula permita
la siguiente funcionalidad:
i. Mostrar la primera mitad de los caracteres de la cadena.
ii. Mostrarla en mayúscula
iii. Mostrar el último carácter.
iv. Mostrarla en forma inversa.
v. Mostrar cada caracter de la cadena separado con un guion.
vi. Mostrar la cantidad de vocales contenidas en la cadena de caracteres. */


public class Ejercicio1{
    public String cadenas;

    public String primeraMitad(){
        return cadenas.substring(0, cadenas.length()/2);
    }

    public String mayuscula(){
        return cadenas.toUpperCase();
    }

    public String ultimoCaracter(){
        return cadenas.substring(cadenas.length() -1);
    }

    public String inversa(){
        String invertida = "";
        for(int i = cadenas.length() - 1;i >= 0;i--){
            invertida = invertida + cadenas.charAt(i);
        }
        return invertida;
    }

    public String separador(){
        String cadeSeparada = "";
        for(int i=0;i<cadenas.length();i++){
            cadeSeparada = cadeSeparada + "-" +cadenas.charAt(i);
        }
        return cadeSeparada;
        /* Otra forma es:
        return cadenas.join("-",cadenas.split(""));*/
    }
    
    public int contadorVocales() {
        int contador = 0;
        String vocales = "AEIOUaeiouÁÉÍÓÚáéíóú";
        for (int i = 0; i < cadenas.length(); i++) {
            for(int j = 0; j < vocales.length(); j++){
                if(cadenas.charAt(i) == vocales.charAt(j)){
                    contador += 1;
                }
            }
        }
        return contador;
    }
}   