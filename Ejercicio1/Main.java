
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        try (Scanner consola = new Scanner(System.in)) {
            System.out.println("Ingrese una cadena de caracteres: ");
            var cadenaCaracteres = consola.nextLine();
            Ejercicio1 cadena1 = new Ejercicio1();
            cadena1.cadenas = cadenaCaracteres;

            System.out.println("\n1 - Primera mitad de los caracteres: "+cadena1.primeraMitad());
            System.out.println("2 - Mayúscula: "+cadena1.mayuscula());
            System.out.println("3 - Ultimo caracter: "+ cadena1.ultimoCaracter());
            System.out.println("4 - Inversa: "+cadena1.inversa());
            System.out.println("5 - Separadas: "+cadena1.separador());
            System.out.println("6 - Contador de vocales: "+cadena1.contadorVocales());
        }
    }
}
