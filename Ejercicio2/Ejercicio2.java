package Ejercicio2;

public class Ejercicio2 {
    float ladoA;
    float ladoB;
    float ladoC;

    public String comprobacion(){
        if(ladoA == ladoB && ladoA == ladoC){
            return "Equilatero";
        }
        else if(ladoA == ladoB && ladoA != ladoC || ladoA != ladoB && ladoA == ladoC){
            return "Isósceles";
        }
        else{
            return "Escaleno";
        }
    }
}
