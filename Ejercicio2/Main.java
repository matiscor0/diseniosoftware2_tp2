/*Ejercicio 2
Elaborar un programa maneje tres valores que representan los lados de un triángulo e imprima el tipo
de triángulo al cual corresponde, teniendo en cuenta que de 
acuerdo a la igualdad o desigualdad de sus lados, los triángulos se clasifican en:
Escaleno: todos sus lados son diferentes.
Isósceles: al menos dos de sus lados son iguales.
Equilátero: los tres lados son iguales. */


package Ejercicio2;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        try (Scanner consola = new Scanner(System.in)) {
            System.out.println("Ingrese el valor del lado A: ");
            float ladoAM = consola.nextFloat();
            System.out.println("Ingrese el valor del lado B: ");
            float ladoBM = consola.nextFloat();
            System.out.println("Ingrese el valor del lado C: ");
            float ladoCM = consola.nextFloat();
            Ejercicio2 calculos = new Ejercicio2();
            calculos.ladoA = ladoAM;
            calculos.ladoB = ladoBM;
            calculos.ladoC = ladoCM;

            System.out.println("Su triangulo es de tipo: "+calculos.comprobacion());
        }
    }
}
