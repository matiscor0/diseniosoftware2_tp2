package Ejercicio3;

public class Ejercicio3 {
    final int EDAD_MINIMA_PARA_VOTAR = 18;
    final int EDAD_MAXIMA_PARA_VOTAR = 120;
    int edadVotante;

    public String comprobarEdad(){
        if(edadVotante >= EDAD_MINIMA_PARA_VOTAR && edadVotante <= EDAD_MAXIMA_PARA_VOTAR){
            return "Votante habilitado";

        } 
        else{
            return "Votante inhabilitado";
        }
    }
}
