/*Ejercicio 3
Codifique un programa que defina dos valores constantes que representan la edad 
mínima y la edad máxima para poder votar. Luego verifique que dada una persona 
se indique si la edad de la misma lo habilita para votar. Muestre por pantalla si el 
mensaje “Votante habilitado” o “Votante inhabilitado en cada caso.
 */
package Ejercicio3;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        try (Scanner consola = new Scanner(System.in)) {
            System.out.println("Ingrese su edad: ");
            int edadEntrada = consola.nextInt();
            Ejercicio3 votante = new Ejercicio3();
            votante.edadVotante = edadEntrada;

            System.out.println(votante.comprobarEdad());
        }
    }
}
