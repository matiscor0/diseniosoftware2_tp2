package Ejercicio4;

public class Ejercicio4 {
    int[] arrayej = new int[10];

    public int par(){
        int banderaPar = 0;

        for(int i=0; i<10; i++){
            if(arrayej[i] % 2 == 0){
                banderaPar += arrayej[i];
            }
        }
        return banderaPar;
    }
    public int impar(){
        int banderaImpar = 0;
        for(int i=0; i<10; i++){
            if(arrayej[i] % 2 != 0){
                banderaImpar += arrayej[i];
            }
        }
        return banderaImpar;
    }
}
