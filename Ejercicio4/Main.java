/*Ejercicio 4
Escribir un programa en el cual se debe definir un array de enteros de 10 posiciones, 
inicializarlo con números enteros, luego definir métodos para que obtenga la 
suma y la cantidad de los elementos pares, la suma y la cantidad de los elementos 
impares, el valor máximo de los elementos pares y el valor mínimo de los elementos impares. 
Mostrar los resultados obtenidos por pantalla */
package Ejercicio4;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Ejercicio4 sumas = new Ejercicio4();
        int[] array = new int[10];
        try (Scanner consola = new Scanner(System.in)) {
            for(int i = 0; i < 10; i++) {
                System.out.println("Ingresa un numero: ");
                array[i] = consola.nextInt();
                sumas.arrayej[i] = array[i];
            }
        }
        System.out.println("Suma de pares: "+sumas.par());
        System.out.println("Suma de impares: "+sumas.impar());
    }
}
