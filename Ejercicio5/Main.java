/*Ejercicio 5
Codifique una clase que tenga atributos que representen el título de una película, 
su género, duración, calificación de acuerdo a la edad (ATP (Aptas para Todo Público), 
PM13 (mayores de trece años), PM16 (mayores de dieciséis) y PM18 (sólo 
para adultos), y director todos los atributos deben ser privados. Defina los métodos 
necesarios para establecer y obtener los valores de los atributos y otro que permita 
mostrar sus valores. Además, defina otra clase que permita instanciar 2 objetos de 
la clase Película y muestre sus datos. */
package Ejercicio5;
public class Main{
    public static void main(String[] args) {
        Pelicula pelicula1 = new Pelicula("Interestelar", "Ciencia ficción/Aventura", 169,"PM13", "Christopher Nolan");
        Pelicula pelicula2 = new Pelicula("Pinocchio","Fantasía/Musical", 114, "ATP", "Guillermo del Toro");


        pelicula1.mostrarInformacion();
        System.out.println("-------------------------");
        pelicula2.mostrarInformacion();
    }
}