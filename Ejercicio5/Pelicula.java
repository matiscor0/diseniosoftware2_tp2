package Ejercicio5;

public class Pelicula {
    private String titulo;
    private String genero;
    private int duracion;
    private String clasificacion;
    private String director;

    public Pelicula(String titulo, String genero, int duracion, String clasificacion, String director) {
        this.titulo = titulo;
        this.genero = genero;
        this.duracion = duracion;
        this.clasificacion = clasificacion;
        this.director = director;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getGenero(){
        return genero;
    }
    public void setGenero(String genero){
        this.genero = genero;
    }
    public String getClasificacion(){
        return clasificacion;
    }
    public void setClasificacion(String clasificacion){
        this.clasificacion = clasificacion;
    }
    public int getDuración(){
        return duracion;
    }
    public void setDuracion(int duracion){
        this.duracion = duracion;
    }
    public String getdirector(){
        return director;
    }
    public void setDirector(String director){
        this.director = director;
    }
    public void mostrarInformacion(){
        System.out.println("Titulo: "+ titulo);
        System.out.println("Genero: "+ genero);
        System.out.println("Clasificacion: "+ clasificacion);
        System.out.println("Duracion: "+ duracion);
        System.out.println("Director: "+ director);
    }
}
