/*Ejercicio 6
 Crear una clase producto, con los siguientes datos: código del producto, 
nombre del mismo, precio de costo, porcentaje de ganancia que se aplica 
sobre el precio de costo, iva del producto, para todos los casos de 21% y 
precio de venta.
 Crear los respectivos métodos accesores para cada atributo. 
 Permitir mostrar la información relativa al producto.
 Permitir determinar el precio de venta, el cual se obtiene de aplicarle el iva al 
resultado del precio de costo + precio de costo * el porcentaje de ganancia.
El método creado no debe ser parte del protocolo de mensajes del objeto
 Indicar cuál de los 2 productos tiene el mayor precio de venta */

package Ejercicio6;

public class Main {
    public static void main(String[] args) {
        Producto producto1 = new Producto(1, "Producto 1", 100.0, 0.1);
        Producto producto2 = new Producto(2, "Producto 2", 50.0, 0.2);

        producto1.mostrarInformacion();
        System.out.println("--------------------------------");
        producto2.mostrarInformacion();
        System.out.println("--------------------------------");
        if(producto1.getPrecioVenta() > producto2.getPrecioVenta()){
            System.out.println(producto1.getNombreProducto()+"; Tiene el mayor precio de venta.");
        }else if(producto1.getPrecioVenta() < producto2.getPrecioVenta()){
            System.out.println(producto2.getNombreProducto()+"; Tiene el mayor precio de venta.");
        }
        else{
            System.out.println(producto1.getNombreProducto()+", Y "+producto2.getNombreProducto()+", Tienen el mismo precio de venta.");
        }

    }
}
