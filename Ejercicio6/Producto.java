package Ejercicio6;

public class Producto {
    private int codigoProd;
    private String nombreProducto;
    private double precioCosto;
    private double porcentajeGanancia;
    private double iva = 0.21;
    private double precioVenta;
    public Producto(int codigoProd, String nombreProducto, double precioCosto, double porcentajeGanancia){
        this.codigoProd = codigoProd;
        this.nombreProducto = nombreProducto;
        this.precioCosto = precioCosto;
        this.porcentajeGanancia = porcentajeGanancia;
        this.calcularPrecioVenta();

    }
    public int getCodigoProducto(){
        return codigoProd;
    }
    public void setCodigoProducto(int codigoProd){
        this.codigoProd = codigoProd;
    }

    public String getNombreProducto(){
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto){
        this.nombreProducto = nombreProducto;
    }
    public double getPrecioProducto(){
        return precioCosto;
    }

    public void setPrecioProducto(double precioProducto){
        this.precioCosto = precioProducto;
    }

    public  double getPorcentajeGanancia(){
        return porcentajeGanancia;
    }
    public void setPorcentajeGanancia(double porcentajeGanancia){
        this.porcentajeGanancia = porcentajeGanancia;
    }
    public double getIva(){
        return iva;
    }
    public void setIva(double iva){
        this.iva = iva;    
    }

    public double getPrecioVenta(){
        return precioVenta;
    }
    public void calcularPrecioVenta(){
        this.precioVenta = this.precioCosto + (this.precioCosto * this.porcentajeGanancia);
        this.precioVenta += this.precioVenta * this.iva;
    }
    public void mostrarInformacion(){
        System.out.println("Código: " + this.codigoProd);
        System.out.println("Nombre: " + this.nombreProducto);
        System.out.println("Precio de costo: $" + this.precioCosto);
        System.out.println("Porcentaje de ganancia: %" + this.porcentajeGanancia);
        System.out.println("IVA: " + this.iva);
        System.out.println("Precio de venta: $" + this.precioVenta);
    }
}
